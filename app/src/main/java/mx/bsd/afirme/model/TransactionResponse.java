package mx.bsd.afirme.model;

public class TransactionResponse {
    private boolean responseCode;
    private String message;

    public boolean isResponseCode() {
        return responseCode;
    }

    public void setResponseCode(boolean responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
