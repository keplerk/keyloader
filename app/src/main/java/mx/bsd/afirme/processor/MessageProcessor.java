package mx.bsd.afirme.processor;

import mx.bsd.afirme.model.DukptInitializeResponse;
import mx.bsd.afirme.model.RandomKeyAnswer;
import mx.bsd.afirme.utils.Constants;

public class MessageProcessor {
    public static Constants.AnswerType getMessageType(String msg) {
        String messageIdentifier = msg.substring(0, 4);

        if (messageIdentifier.indexOf("3") == 0) {
            String transactionId = msg.substring(12, 14);
            switch (messageIdentifier) {
                case "3010" :
                    if (transactionId.equals("12")) {
                        return Constants.AnswerType.ANSWER_RANDOM_KEY_REQUEST;
                    }
                    if (transactionId.equals("13")) {
                        return Constants.AnswerType.ANSWER_DUKPT_INSTALLATION;
                    }
                    break;
                default :
                    return Constants.AnswerType.NONE;
            }
        }

        return Constants.AnswerType.NONE;
    }

    private static int nextStringIndex = 0;
    private static String message;
    public static <E> E processMessage(Constants.AnswerType messageType, String msg) {
        nextStringIndex = 0;
        message = msg;

        switch (messageType) {
            case ANSWER_RANDOM_KEY_REQUEST:
                RandomKeyAnswer randomKeyAnswer = new RandomKeyAnswer();
                randomKeyAnswer.setMessageIdentifier(getNextString(4));
                randomKeyAnswer.setMessageLength(getNextString(4));
                randomKeyAnswer.setStatus(getNextString(4));
                randomKeyAnswer.setTransacitonId(getNextString(2));
                randomKeyAnswer.setSerialNumber(getNextString(11));
                randomKeyAnswer.setEncryptedBlockLength(getNextString(4));
                randomKeyAnswer.setEncryptedBlock(getNextString(Integer.valueOf(randomKeyAnswer.getEncryptedBlockLength()) * 2));
                randomKeyAnswer.setRandomKeyKcv(getNextString(6));
                randomKeyAnswer.setEncryptedBlockCrc32(getNextString(8));

                return (E)randomKeyAnswer;

            case ANSWER_DUKPT_INSTALLATION:
                DukptInitializeResponse dukptInitializeResponse = new DukptInitializeResponse();
                dukptInitializeResponse.setMessageIdentifier(getNextString(4));
                dukptInitializeResponse.setMessageLength(getNextString(4));
                dukptInitializeResponse.setStatus(getNextString(4));
                dukptInitializeResponse.setTransactionId(getNextString(2));
                dukptInitializeResponse.setSerialNumber(getNextString(2));

                return (E)dukptInitializeResponse;
        }

        return null;
    }

    private static String getNextString (int length) {
        String res = message.substring(nextStringIndex, nextStringIndex + length);
        nextStringIndex += length;
        return res;
    }
}
