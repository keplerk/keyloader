package mx.bsd.afirme.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mx.bsd.afirme.R;
import mx.bsd.afirme.utils.Constants;
import mx.bsd.afirme.utils.Messages;
import mx.bsd.afirme.utils.Pinpad;
import mx.bsd.afirme.utils.Utils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Utils.activity = this;
        setEvents();
    }

    Button btnLoad;
    TextView txtKey;

    private void setEvents () {
        btnLoad = (Button)findViewById(R.id.btnLoad);
        txtKey = (TextView) findViewById(R.id.txtKey);

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (txtKey.getText().length() < 512) {
//                    Utils.ShowToast("La llave debe medir exactamente 512 posiciones");
//                    return;
//                }
                Utils.StartProcessDialog("Conectando con pinpad");
                Messages.buildDukptRequest("");
                Pinpad.getInstance().startService();
            }
        });
    }
}