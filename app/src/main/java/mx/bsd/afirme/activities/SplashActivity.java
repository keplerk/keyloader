package mx.bsd.afirme.activities;

import android.app.Activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import mx.bsd.afirme.R;

public class SplashActivity extends Activity {
    private static int SPLASH_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_activity);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME);
    }
}
