package mx.bsd.afirme.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.itos.pagomovil.service.PinpadService;
import mx.bsd.afirme.model.DukptInitializeResponse;
import mx.bsd.afirme.model.DukptInstallRequest;
import mx.bsd.afirme.model.RandomKeyAnswer;
import mx.bsd.afirme.model.TransactionResponse;
import mx.bsd.afirme.processor.MessageProcessor;
import mx.bsd.afirme.tasks.InitializeTask;
import mx.bsd.afirme.tasks.actions.IStompEvents;
import mx.bsd.afirme.utils.Constants;
import mx.bsd.afirme.utils.Messages;
import mx.bsd.afirme.utils.Pinpad;
import mx.bsd.afirme.utils.Utils;

public class PinpadReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            final String action = intent.getAction();
            int status = intent.getIntExtra(PinpadService.EXTRA_STATUS, -1);
            final String data = intent.getStringExtra("es.itos.pinpad.intent.extra.DATA");
            final byte[] byteMsg = intent.getByteArrayExtra("es.itos.pinpad.intent.extra.MSG");
            final String message = byteMsg == null ? "" : new String(byteMsg);

            switch (action) {
                case PinpadService.ACTION_OPERATION_END:
                    switch (status) {
                        case PinpadService.STATUS_ERROR_BLUETOOTH_NOT_ENABLED:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_BLUETOOTH_NOT_ENABLED);
                            break;
                        case PinpadService.STATUS_ERROR_COMMUNICATION_ERROR:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_COMMUNICATION_ERROR);
                            break;
                        case PinpadService.STATUS_ERROR_INVALID_OPERATION_DATA:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_INVALID_OPERATION_DATA);
                            break;
                        case PinpadService.STATUS_ERROR:
                            Utils.StopProcessDialog();
                            final Intent intentError = new Intent(PinpadService.ACTION_CANCEL_OPERATION_REQUEST);
                            context.startService(intentError);
                            break;
                        case PinpadService.STATUS_ERROR_PINPAD_NOT_READY:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_PINPAD_NOT_READY);
                            break;
                        case PinpadService.STATUS_OPERATION_CANCELLED:
                            Utils.StopProcessDialog();
                            Pinpad.getInstance().stopService();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_OPERATION_CANCELED);
                            break;
                        case PinpadService.STATUS_ERROR_TIME_OUT:
                            Utils.StopProcessDialog();
                            Pinpad.getInstance().stopService();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_TIMEOUT);
                            break;


                        case PinpadService.STATUS_READY:
                            switch (Pinpad.getInstance().getMesssageType()) {
                                case REQUEST_RANDOM_KEY:
                                    Utils.UpdateProcessDialogMessage("Cargando llave de inicialización");
                                    Pinpad.getInstance().sendRequest();
                                    break;
                                case REQUEST_DUKPT_INSTALL:
                                    Pinpad.getInstance().sendRequest();
                                    break;
                            }
                            break;


                        case PinpadService.STATUS_FINISH:
                            Pinpad.getInstance().stopService();
                            Constants.AnswerType answer = MessageProcessor.getMessageType(data);
                            switch (answer) {
                                case ANSWER_RANDOM_KEY_REQUEST:
                                    Utils.UpdateProcessDialogMessage("Solicitando llave DUKPT");
                                    RandomKeyAnswer randomKeyAnswer = MessageProcessor.processMessage(answer, data);

                                    new InitializeTask(new IStompEvents() {
                                        @Override
                                        public void onComplete() {
                                            Utils.logI("Stop conectado");
                                        }

                                        @Override
                                        public void onError(Throwable ex) {
                                            Utils.logE((Exception)ex);
                                        }

                                        @Override
                                        public void onNext(String stompMessage) {
                                            try {
                                                Utils.UpdateProcessDialogMessage("Installando llave DUKPT");
                                                if (stompMessage.contains("false")) {
                                                    TransactionResponse response = new ObjectMapper().readValue(stompMessage, TransactionResponse.class);
                                                    Utils.StopProcessDialog();
                                                    Utils.ShowToast(response.getMessage());
                                                    return;
                                                }
                                                DukptInstallRequest dukpt = new ObjectMapper().readValue(stompMessage, DukptInstallRequest.class);
                                                Messages.buildDukptInstallation(dukpt);
                                                Pinpad.getInstance().startService();
                                            } catch (Exception e) {
                                                Utils.logE(e);
                                            }
                                        }
                                    }).execute(randomKeyAnswer);
                                    break;
                                case ANSWER_DUKPT_INSTALLATION:
                                    DukptInitializeResponse response = MessageProcessor.processMessage(answer, data);
                                    Utils.StopProcessDialog();

                                    if (!response.getStatus().equals("0000")) {
                                        Utils.ShowToast("Error al intentar cargar la llave");
                                    } else {
                                        Utils.ShowToast("Llave cargada correctamente");
                                    }
                                    break;
                            }
                            break;
                    }
                    break;

            }
        } catch (Exception ex) {
            Pinpad.getInstance().stopService();
            Utils.StopProcessDialog();
            Utils.logE(ex);
        }
    }
}
