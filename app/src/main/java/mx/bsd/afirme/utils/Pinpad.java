package mx.bsd.afirme.utils;

import android.content.Intent;

import es.itos.pagomovil.service.PinpadService;

public class Pinpad {
    private static Pinpad mPinpad;
    public static Pinpad getInstance () {
        if (mPinpad == null) {
            mPinpad = new Pinpad();
        }

        return mPinpad;
    }

    public void startService() {
        PinpadService.start(Utils.activity.getApplicationContext());
    }

    public void stopService() {
        PinpadService.stop(Utils.activity.getApplicationContext());
    }

    private Constants.MessageType messsageType;

    public Constants.MessageType getMesssageType() {
        return messsageType;
    }

    public void setMesssageType(Constants.MessageType messsageType) {
        this.messsageType = messsageType;
    }

    private String currentMessage;
    public void setCurrentMessage(String currentMessage) {
        this.currentMessage = currentMessage;
    }

    public void sendRequest() {
        final Intent intent = new Intent(Utils.activity, PinpadService.class);
        intent.setAction(PinpadService.ACTION_OPERATION_REQUEST);
        intent.putExtra(PinpadService.EXTRA_MSG, currentMessage.getBytes());
        Utils.activity.startService(intent);

        this.setMesssageType(Constants.MessageType.NONE);
    }
}