package mx.bsd.afirme.utils;

import es.itos.pagomovil.pinpad.Message;

public class Constants {
    public static final String LOG_NAME = "BSD_PAYMENT_APP";
    public static final String RSA_EXP_LENGTH = "6";
    public static final String RSA_EXP = "010001";

    public static final String LLAVE = "DF71C129AC324DF0144D6A4A6383E81EF0E5510ECF658523764D84BE0E3C6B02EF94679C9680EAC1C4A28323CEB911A2431EDB5CE14DC07F47A7BF94875963188F27B4C20E7C2CE8F0D5113260970178CB3ACAEB4F2ACCB42F0203BDDAAB383F4C0552E480900CF9FF10C10D89F7EB4AA72A9AF97DB280E82C0137E2D4CC9C9CDC990C7A7D458815403600681A210162723976A596F91D3C8A5D5C946754C3114E74074F3ACCEFD951201C6CD454220E365AC8F5D6ACCE76BD997DE4B690EA9F5318C53FB19650D37F499721C13239F21EB5849A434D219C2D7D4DA27875FCB64638F8FDF12CA363CE5D5E4E7C4698CD6FC64F9C9BCB006BC5399AEA349E7091";

    public static final int PINPAD_REQUEST = Message.PARAMETERS_UPDATE;

    public enum MessageType {
        NONE, REQUEST_RANDOM_KEY, REQUEST_DUKPT_INSTALL
    }

    public enum AnswerType {
        ANSWER_RANDOM_KEY_REQUEST, ANSWER_DUKPT_INSTALLATION, NONE
    }

    public class Error {
        public static final String STATUS_ERROR_BLUETOOTH_NOT_ENABLED = "Error : Bluetooth no activado";
        public static final String STATUS_ERROR_COMMUNICATION_ERROR = "Error : No se pudo establecer comunicación con Pinpad";
        public static final String STATUS_ERROR_INVALID_OPERATION_DATA = "Error : Ocurrio de datos con el pinpad";
        public static final String STATUS_ERROR_PINPAD_NOT_READY = "Error : Pinpad no listo";
        public static final String STATUS_ERROR_TIMEOUT = "Error : Tiempo de espera agotado, no se pudo conectar con pinpad";
        public static final String STATUS_ERROR_OPERATION_CANCELED = "Operación cancelada";
        public static final String STATUS_ERROR_PINPAD_BUSY = "Error : Pinpad ocupado";
    }

    public class TransactionCodes {
        public static final String DUKPT_KEY_GENERATE = "12";
        public static final String DUKPT_KEY_INSTALL = "13";
    }

    public static final String BASE_REST_URL = "http://devbroker.bsdap.com:8080/SwitchProsa/";
    //public static final String BASE_REST_URL = "http://192.168.1.66:8080/";
    public static final String BASE_STOMP_URL = "ws://52.8.215.37:15674/ws";
}
