package mx.bsd.afirme.utils;

import mx.bsd.afirme.model.DukptInstallRequest;

public class Messages {
    public static void buildDukptRequest(String key) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_RANDOM_KEY);

        String messageIdentifier    = Integer.valueOf(Constants.PINPAD_REQUEST).toString();
        String messageLength        = "";
        String transactionId        = Constants.TransactionCodes.DUKPT_KEY_GENERATE;
        String rsaPkLen             = String.format("%04d", key.length());
        String rsaPk                = key;
        String rsaExpLen            = Constants.RSA_EXP_LENGTH;
        String rsaExp               = Constants.RSA_EXP;

        Integer msgLength = messageIdentifier.length()
                + 4
                + transactionId.length()
                + rsaPkLen.length()
                + rsaPk.length()
                + rsaExpLen.length()
                + rsaExp.length();

        messageLength = String.format("%04d", msgLength);
        String msg = "";

        if (!key.equals("")) {
            msg = messageIdentifier
                    + messageLength
                    + transactionId
                    + rsaPkLen
                    + rsaPk
                    + rsaExpLen
                    + rsaExp;
        } else {
            msg = messageIdentifier
                    + "0010"
                    + transactionId;
        }

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildDukptInstallation(DukptInstallRequest dukpt) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_DUKPT_INSTALL);

        String messageIdentifier    = Integer.valueOf(Constants.PINPAD_REQUEST).toString();
        String messageLength        = String.format("%04d", 62 + dukpt.getCheckValue().length());
        String transactionId        = Constants.TransactionCodes.DUKPT_KEY_INSTALL;
        String encriptedDukptKey    = dukpt.getBdk();
        String ksn                  = dukpt.getKsn();
        String dukptKcv             = dukpt.getCheckValue();

        String msg  = messageIdentifier
                + messageLength
                + transactionId
                + encriptedDukptKey
                + ksn
                + dukptKcv;

        Pinpad.getInstance().setCurrentMessage(msg);
    }
}
