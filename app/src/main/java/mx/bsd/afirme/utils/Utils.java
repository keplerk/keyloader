package mx.bsd.afirme.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.UUID;

import mx.bsd.afirme.activities.MainActivity;

public class Utils {

    public static MainActivity activity;

    private static String[] permissionsArray = new String[]{
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WAKE_LOCK
    };

    public static void requestPermissions(Activity context){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            if (!checkPermissionsGranted(context, permissionsArray)) {
                ActivityCompat.requestPermissions(context, permissionsArray, 1);
            }
        }
    }

    private static boolean checkPermissionsGranted(Activity context,String [] permissionsArray){
        boolean result=true;

        for(String permission:permissionsArray){
            if (!checkStatusPermission(context,permission)) {
                result=false;
            }
        }
        return  result;
    }

    private static boolean checkStatusPermission(Activity context,String permission){
        boolean result=true;
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            result=false;
        }
        return result;
    }

    private static String uuid;
    public static String GetUuid (Boolean generateNew) {
        if (generateNew) {
            uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
        }

        return uuid;
    }

    public static void ShowToast (final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static final void logD(String msg) {
        Log.d(Constants.LOG_NAME, msg);
    }

    public static final void logV(String msg) {
        Log.v(Constants.LOG_NAME, msg);
    }

    public static final void logW(String msg) {
        Log.w(Constants.LOG_NAME, msg);
    }

    public static final void logE(Exception ex) {
        Log.e(Constants.LOG_NAME, "[" + ex.toString() + "] " + ex.getMessage());
    }

    public static final void logI(String msg) {
        Log.i(Constants.LOG_NAME, msg);
    }

    private static ProgressDialog mProgressDialog;

    public static void StartProcessDialog (final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = new ProgressDialog(activity);
                mProgressDialog.setMessage(msg);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        return true;
                    }
                });
                mProgressDialog.show();
            }
        });
    }

    public static void StartProcessDialog (final Activity activity, final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = new ProgressDialog(activity);
                mProgressDialog.setMessage(msg);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        return true;
                    }
                });
                mProgressDialog.show();
            }
        });
    }

    public static void UpdateProcessDialogMessage (final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.setMessage(msg);
            }
        });
    }

    public static void StopProcessDialog () {
        mProgressDialog.dismiss();
    }
}
